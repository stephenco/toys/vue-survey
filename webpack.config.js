const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

let config = {
  mode: 'development',
  entry: {
    vue: 'vue',
    index: './src/index.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve('dist')
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            js: [ 'babel-loader' ],
            css: [ 'vue-style-loader', { loader: 'css-loader' }],
            scss: 'vue-style-loader!css-loader!sass-loader', // <style lang="scss">
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax' // <style lang="sass">
          },
          esModule: true,
          cacheBusting: true
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          path.resolve('src'),
          path.resolve('node_modules/webpack-dev-server/client')
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  },
  devServer: {
    host: '0.0.0.0',
    port: 8010,
    compress: true,
    open: false,
    watchOptions: {
      ignored: /node_modules/,
      poll: true
    },
    publicPath: '/dist/',
    contentBase: path.resolve('src/assets'),
    watchContentBase: true
  },
  plugins: [
      new webpack.NamedModulesPlugin(),
      // Exchanges, adds, or removes modules while an application is running, without a full reload.
      new webpack.HotModuleReplacementPlugin(),
      new VueLoaderPlugin()
  ],
  resolve: {
      /**
       * The compiler-included build of vue which allows to use vue templates
       * without pre-compiling them
       */
      alias: {
          'vue$': 'vue/dist/vue.esm.js',
      },
      extensions: ['*', '.vue', '.js', '.json'],
  },
  // webpack outputs performance related stuff in the browser.
  // performance: {
  //     hints: false,
  // },
};

module.exports = config;
