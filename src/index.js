import Vue from 'vue';
import Survey from './views/survey';

import '../node_modules/animate.css/animate.min.css';
import 'es6-promise/auto'

new Vue({
  el: '#survey',
  name: 'survey-app',
  template: $('#survey').html(),
  components: { Survey }
});
