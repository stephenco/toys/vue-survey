import Vue from 'vue';
import Vuex from 'vuex';
import getSurvey from '../survey-contents';

Vue.use(Vuex);

function notify(state) {
  if (!state || !state.subscribers || !state.slides) { return; }

  function func(obj) {
    if (!obj) { return null; }
    return ({}.toString.call(obj) === '[object Function]') ? obj : eval(obj);
  }

  function defined(obj) {
    return typeof obj != 'undefined';
  }

  var data = {
    metadata: state.metadata,
    selections: {}
  };

  for (var key in state.slides) {
    var slide = state.slides[key];
    if (!defined(slide.selection)) { continue; }
    var mapKey = (slide.meta || {}).storageKey || key;
    data.selections[mapKey] = slide.selection;
  }

  for (var i = 0; i < state.subscribers.length; i++) {
    try { func(state.subscribers[i])(data); }
    catch (error) { console.warn('subscriber callback error', error); }
  }
}

export default new Vuex.Store({
  state: {
    survey: {},
    slides: {},
    metadata: {},
    subscribers: []
  },
  getters: {
    slide: (state) => (name) => {
      var survey = state.survey;
      var current = survey.slidesArray[survey.currentSlideIndex];
      return name ? state.slides[name] || state.slides[current] : state.slides[current];
    },

    isCurrent: (state) => (name) => {
      var survey = state.survey;
      return survey.slidesArray[survey.currentSlideIndex] == name;
    },

    runningValue: (state) => (name) => {
      return (function collect(slide) {
        if (!slide) { return 0; }
        var value = slide.runningValue || 0;
        return slide.previous ? collect(state.slides[slide.previous]) + value : value;
      })(state.slides[name]);
    },

    text: (state, getters) => (text) => {
      if (!text) { return text; }

      var cache = {};
      function query(q) {
        if (!q) { return ''; }
        if (cache[q]) { return cache[q]; }

        function find(key) {
          if (state.metadata[key]) { return state.metadata[key]; }
          if (cache[key]) { return cache[key]; }
          var value = '** MISSING VALUE **';

          if (key == 'runningValue') {
            value = getters.runningValue(state.survey.slidesArray[state.survey.currentSlideIndex]);

          } else if (/^['"].+['"]$/.test(key)) {
            value = key.substring(1, key.length - 1);

          } else if (NaN != Number(key)) {
            value = Number(key);
          }

          cache[key] = value;
          return value;
        }

        function num(str) {
          return str ? Number(str) : 0;
        }

        var operators = {
          add: function(a, b) { return num(a) + num(b); },
          subtract: function(a, b) { return num(a) - num(b); }
        };

        var formatters = {
          '$': function(value) {
            return (new Intl.NumberFormat('en-US', {
              style: 'currency', currency: 'USD', minimumFractionDigits: 0
            })).format(value || 0);
          }
        };

        var bits = q.split('|');
        var op = operators[bits[0]];
        var value = op ? op(find(bits[1]), find(bits[2])) : find(bits[0]);

        var fm = bits.length > 1 ? formatters[bits[bits.length - 1]] : null;
        cache[q] = fm ? fm(value) : value;

        return cache[q];
      }

      var rex = /({{.+?}})/g
      var result = text;
      var match = rex.exec(result);

      while (match) {
        var target = match[0];
        var q = target.replace(/[{}]/ig, '');
        result = result.replace(target, query(q));
        match = rex.exec(result);
      }

      return result;
    }
  },
  mutations: {
    initSurvey(state, type, metadata) {
      var data = getSurvey(type);

      var survey = {
        title: data.title || "Survey",
        currentSlideIndex: 0,
        slidesArray: []
      };

      if (data.onCompleted) {
        state.subscribers.push(data.onCompleted);
      }

      state.slides = {};
      for (var i = 0; i < data.slides.length; i++) {
        var slide = data.slides[i];
        survey.slidesArray[survey.slidesArray.length] = slide.name;
        state.slides[slide.name] = slide;
      }

      state.metadata = metadata || {};
      for (var key in data.meta) {
        if (state.metadata[key]) { continue; }
        state.metadata[key] = data.meta[key];
      }

      state.survey = survey;
    },

    updateSlide(state, values) {
      if (!values || !values.name) { return; }

      var slide = state.slides[values.name];
      if (!slide) { return; }

      slide.selection = values.selection;
      slide.text = values.text || slide.text;
      slide.runningValue = values.runningValue;
      slide.next = values.next;

      if (!slide.notes) { slide.notes = {}; }
      if (values.valueNote) { slide.notes.value = values.valueNote; }
      if (values.footnote) { slide.notes.text = values.footnote; }
    },

    addMeta(state, obj) {
      for (var key in obj || {}) {
        state.metadata[key] = obj[key];
      }
    },

    nextSlide(state) {
      var survey = state.survey, slides = state.slides;
      var current = slides[survey.slidesArray[survey.currentSlideIndex]];
      if (!current) { return; }

      current.state = 'exiting-left';

      var next = current.next ? slides[current.next] :
        slides[survey.slidesArray[survey.currentSlideIndex + 1]];

      if (next) {
        next.previous = current.name;
        next.state = 'entering-right';
        for (var i = 0; i < survey.slidesArray.length; i++) {
          if (survey.slidesArray[i] == next.name) {
            survey.currentSlideIndex = i;
            break;
          }
        }
      } else {
        survey.currentSlideIndex = -1;
        notify(state);
      }
    },

    previousSlide(state) {
      var survey = state.survey, slides = state.slides;
      var current = slides[survey.slidesArray[survey.currentSlideIndex]];
      if (!current) { return; }

      current.state = 'exiting-right';

      var previous = current.previous ? slides[current.previous] :
        slides[survey.slidesArray[survey.currentSlideIndex - 1]];

      if (previous) {
        previous.state = 'entering-left';
        for (var i = 0; i < survey.slidesArray.length; i++) {
          if (survey.slidesArray[i] == previous.name) {
            survey.currentSlideIndex = i;
            break;
          }
        }
      }
    },

    setSlide(state, name) {
      if (!name) { return; }

      var survey = state.survey;
      var slides = state.slides;
      var slide = state.slides[name];
      if (!slide) { console.warn('could not set slide', name); return; }

      for (var i = 0; i < survey.slidesArray.length; i++) {
        if (survey.slidesArray[i] == slide.name) {
          survey.currentSlideIndex = i;
          break;
        }
      }
    },

    subscribe(state, callback) {
      if (!callback) { return; }
      state.subscribers.push(callback);
    },

    done(state) {
      notify(state);
    }

  }
});
