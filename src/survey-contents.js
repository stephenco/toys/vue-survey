import RunningTotalSurvey from './running-total-survey';

export default function getSurvey(name) {
  var survey = new RunningTotalSurvey("Scholarship").addSlides([{
    name: "start",
    caption: "Innate Scholarship",
    title: "Congratulations, you've successfully submitted your scholarship application.",
    text: "Want a chance to reduce your college debt even more?",
    inputs: [
      { button: "yes please", addValue: 0, next: "what-type-student" },
      { button: "no thanks", next: "quit" }
    ]
  }, {
    name: "returning-start",
    caption: "Innate Scholarship",
    title: "Welcome back! You've successfully submitted your scholarship application.",
    text: "Want a chance to reduce your college debt even more?",
    inputs: [
      { button: "yes please", addValue: 0, next: "what-type-student", addMeta: { returning: true } },
      { button: "no thanks", next: "quit" }
    ]
  }, {
    name: "what-type-student",
    caption: "Getting Started (test only)",
    title: "Are you a high school or college student?",
    text: "Fact: a public in-state college is significantly cheaper than a public out-of-state college.",
    meta: { storageKey: "student-type", bypass: "onDefault" },
    inputs: [
      { option: "High School Student", addValue: 0, defaultIf: "highschoolStudent", next: "highschool-1" },
      { option: "College Student", addValue: 0, defaultIf: "collegeStudent", next: "college-1" }
    ]
  }, {
    name: "highschool-1",
    caption: "1/4",
    title: "What type of college would you like to attend?",
    meta: { storageKey: "college-type" },
    inputs: [
      { option: "Public - In State", addValue: 88080, addMeta: { savings: 35232, schoolType: "public in-state" }, defaultIf: "always", changeValueNote: "this is 4-year average cost", next: "highschool-2" },
      { option: "Public - Out Of State", addValue: 150680, addMeta: { savings: 35232, schoolType: "public out-of-state" }, changeValueNote: "this is the 4-year average cost", next: "highschool-2" },
      { option: "Private", addValue: 192640, addMeta: { savings: 35232, schoolType: "private" }, changeValueNote: "this is the 4-year average cost", next: "highschool-2" },
    ]
  }, {
    name: "highschool-2",
    caption: "2/4",
    title: "How much have you saved for college?",
    text: "Enter everything you've saved, plus any financial assistence you expect from parents or family. Do not include scholarship or grants.",
    meta: { storageKey: "has-savings" },
    inputs: [
      { text: "money", subtractValue: "text", next: "highschool-3" }
    ]
  }, {
    name: "highschool-3",
    caption: "3/4",
    title: "How much do you expect from scholarships or grants?",
    text: "Enter approximately how much you expect to recieve in scholarships and grants over your four years in college.",
    meta: { storageKey: "expects-scholarships" },
    inputs: [
      { text: "money", subtractValue: "text", next: "highschool-4" }
    ]
  }, {
    name: "highschool-4",
    caption: "4/4",
    title: "On average, a four year college degree now takes up to 5.6 years.",
    text: "At a {{schoolType}} school that extra 1.6 years will cost you an additional {{savings|$}}.",
    notes: { text: "*NSC Research Center, Signature Report 11 (2016)" },
    inputs: [
      { button: "continue", next: "shared-end" }
    ]
  }, {
    name: "college-1",
    caption: "1/5",
    title: "What type of college do you attend?",
    meta: { storageKey: "college-type" },
    inputs: [
      { option: "Public - In State", addValue: 88080, addMeta: { savings: 35232, schoolType: "public in-state" }, defaultIf: "always", changeValueNote: "this is the 4-year average cost", next: "college-2" },
      { option: "Public - Out Of State", addValue: 150680, addMeta: { savings: 35232, schoolType: "public out-of-state" }, changeValueNote: "this is the 4-year average cost", next: "college-2" },
      { option: "Private", addValue: 192640, addMeta: { savings: 35232, schoolType: "private" }, changeValueNote: "this is the 4-year average cost", next: "college-2" },
    ]
  }, {
    name: "college-2",
    caption: "2/5",
    title: "How much have you already paid for college?",
    text: "Enter the total amount of money you have paid your college so far. Include all sources, like college savings, student loans, scholarships and grants.",
    meta: { storageKey: "already-paid" },
    inputs: [
      { text: "money", addValue: "text", next: "college-3" }
    ]
  }, {
    name: "college-3",
    caption: "3/5",
    title: "How much have you saved to complete college?",
    text: "Enter everything you've saved, plus any financial assistance you expect from parents or family. Do not include scholarships or grants.",
    meta: { storageKey: "has-savings" },
    inputs: [
      { text: "money", subtractValue: "text", next: "college-4" }
    ]
  }, {
    name: "college-4",
    caption: "4/5",
    title: "How much more do you expect from scholarships or grants?",
    text: "Enter approximately how much you expect to receive in scholarships and grants for your remaining time in college.",
    meta: { storageKey: "expects-scholarships" },
    inputs: [
      { text: "money", subtractValue: "text", next: "college-5" }
    ]
  }, {
    name: "college-5",
    caption: "5/5",
    title: "On average, a four year college degree now takes up to 5.6 years.",
    text: "At a {{schoolType}} school that extra 1.6 years will cost you an additional {{savings|$}}.",
    notes: { text: "*NSC Research Center, Signature Report 11 (2016)" },
    inputs: [
      { button: "continue", next: "shared-end" }
    ]
  }, {
    name: "shared-end",
    caption: "The Innate Guarantee",
    title: "We can save you {{savings|$}} and 1.6 years for just $39.",
    text: "Learn how to reduce your college debt by {{savings|$}}.",
    inputs: [
      { button: "yes please", addMeta: { buyUpgrade: true }, next: "quit" },
      { button: "no thanks", addMeta: { buyUpgrade: false }, next: "quit" } // TODO: QUESTION: customer wants an additional screen for "second chance" ?
    ]
  }]);

  switch (name) {
    case "highschool":
    case "college":
      survey.meta[name + "Student"] = true;
      return survey;

    default:
      return null;
  }
}
