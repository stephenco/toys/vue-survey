export default function RunningTotalSurvey(title, meta, onCompleted) {
  this.Slide = function(obj) {
    this.name = null;
    this.caption = null;
    this.next = null;
    this.previous = null;
    this.title = null;
    this.text = null;
    this.inputs = {};
    this.meta = {};
    this.notes = {};
    this.inputValue = null;
    this.runningValue = 0;

    for (var key in obj) {
      this[key] = obj[key];
    }
  };

  this.title = title || "";
  this.meta = meta || {};
  this.onCompleted = this.onCompleted;

  this.slides = [];
  this.currentSlideIndex = 0;

  var self = this;

  this.addSlides = function(list) {
    for (var i = 0; i < list.length; i++) {
      self.slides[self.slides.length] = new self.Slide(list[i]);;
    }
    return self;
  }
}
